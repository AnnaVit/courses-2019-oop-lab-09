package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix {

	private final int nthread;
	
	/**
	 * @param nthread
	 */
	public MultiThreadedSumMatrix(final int nthread) {
		this.nthread = nthread;
	}

	private static class Worker extends Thread{
		private final double [][] matrix;
		private final int startpos;
		private final int nelem;
		private long res;
		
		/**
		 * @param matrix
		 * @param startpos
		 * @param nelem
		 */
		Worker(final double [][] matrix, final int startpos, final int nelem){
			this.matrix = matrix;
			this.startpos = startpos;
			this.nelem = nelem;
		}
		
		public void run() {
			System.out.println("Working from position " + this.startpos + " to position " + (this.startpos + this.nelem - 1));
			for(int d = this.startpos; d < this.matrix.length && d < this.startpos + this.nelem; d++) {
				for(int i=0;i < this.matrix.length; i++) {
					this.res += this.matrix[d][i];
				}
			}
		}
		
		public long getResult() {
			return this.res;
		}
		
	}
	
	@Override
	public double sum(double[][] matrix) {
		final int size = matrix.length % this.nthread + matrix.length / this.nthread;
		
		final List<Worker> workers = new ArrayList<>(nthread);
		for(int start = 0; start < matrix.length; start += size) {
			workers.add(new Worker(matrix,start,size));
		}
		
		for(final Worker w : workers) {
			w.start();
		}
		
		long sum = 0;
		for(final Worker w : workers) {
			try {
				w.join();
				sum += w.getResult();
			}catch(InterruptedException e) {
				throw new IllegalStateException(e);
			}
		}
		
	return sum;	
		
	}

}
